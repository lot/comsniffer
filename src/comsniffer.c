#define _CRT_SECURE_NO_WARNINGS

#include <libserialport.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <time.h>

#define BUFFER_SIZE 65536

enum rw_buffer_state { BUFFER_FULL = 1 };

struct rw_buffer {
	unsigned char data[BUFFER_SIZE];
	size_t head;
	size_t tail;
	enum rw_buffer_state state;
};

void init_buffer(struct rw_buffer* buf) {
	buf->head = 0;
	buf->tail = 0;
	buf->state = 0;
}

size_t buffer_max_write(struct rw_buffer* buf) {
	assert(buf->head < BUFFER_SIZE && buf->tail < BUFFER_SIZE);
	if (buf->state & BUFFER_FULL) return 0;
	if (buf->head < buf->tail) return buf->tail - buf->head;
	return BUFFER_SIZE - buf->head;
}

void buffer_bytes_written(struct rw_buffer* buf, size_t n) {
	assert(buf->head < BUFFER_SIZE && buf->tail < BUFFER_SIZE);
	assert(n <= buffer_max_write(buf));
	if (n == 0) return;
	buf->head += n;
	if (buf->head == BUFFER_SIZE) buf->head = 0;
	if (buf->head == buf->tail) buf->state |= BUFFER_FULL;
}

size_t buffer_max_read(struct rw_buffer* buf) {
	assert(buf->head < BUFFER_SIZE && buf->tail < BUFFER_SIZE);
	if (buf->tail == buf->head && !(buf->state & BUFFER_FULL)) return 0;
	if (buf->tail < buf->head) return buf->head - buf->tail;
	return BUFFER_SIZE - buf->tail;
}

void buffer_bytes_read(struct rw_buffer* buf, size_t n) {
	assert(buf->head < BUFFER_SIZE && buf->tail < BUFFER_SIZE);
	assert(n <= buffer_max_read(buf));
	buf->tail += n;
	if (buf->tail == BUFFER_SIZE) buf->tail = 0;
	if (n > 0) buf->state &= ~BUFFER_FULL;
}

int read_some(struct sp_port* port, struct rw_buffer* buf) {
	size_t n = buffer_max_write(buf);
	if (n == 0) return 0;
	int result = sp_nonblocking_read(port, buf->data + buf->head, n);
	if (result < 0) return result;
	buffer_bytes_written(buf, result);
	return result;
}

int write_some(struct sp_port* port, struct rw_buffer* buf) {
	size_t n = buffer_max_read(buf);
	if (n == 0) return 0;
	int result = sp_nonblocking_write(port, buf->data + buf->tail, n);
	if (result < 0) return result;
	buffer_bytes_read(buf, result);
	return result;
}

void print_usage(struct sp_port** ports) {
	printf("Usage: comsniffer <port 1> <port 2>\nAvailable ports:\n");
	size_t i;
	for (i = 0; ports[i] != NULL; ++i) {
		printf("\t%s\n", sp_get_port_name(ports[i]));
	}
}

int open(struct sp_port* port) {
	int result = 0;
	if (sp_open(port, SP_MODE_READ_WRITE) != SP_OK) {
		fprintf(stderr, "Failed to open serial port.\n");
		result = 1;
		goto out;
	}

	if (sp_set_baudrate(port, 9600) != SP_OK) {
		result = 1;
		goto error_settings;
	}

	if (sp_set_parity(port, SP_PARITY_NONE) != SP_OK) {
		result = 1;
		goto error_settings;
	}

	if (sp_set_bits(port, 8) != SP_OK) {
		result = 1;
		goto error_settings;
	}

	if (sp_set_stopbits(port, 1) != SP_OK) {
		result = 1;
		goto error_settings;
	}

	goto out;

error_settings:
	fprintf(stderr, "Failed to set correct serial port connection settings.\n");
	sp_close(port);
out:
	return result;
}

int main(int argc, char** argv) {
	struct sp_port** ports;
	if (sp_list_ports(&ports) != SP_OK) return 1;

	if (argc != 3) {
		print_usage(ports);
		return 1;
	}

	struct sp_port* port1 = NULL;
	struct sp_port* port2 = NULL;

	size_t i;
	for (i = 0; ports[i] != NULL; ++i) {
		const char* name = sp_get_port_name(ports[i]);
		if (strcmp(argv[1], name) == 0) port1 = ports[i];
		else if (strcmp(argv[2], name) == 0) port2 = ports[i];
	}

	if (port1 == NULL || port2 == NULL) {
		print_usage(ports);
		return 1;
	}

	const char* port1_name = sp_get_port_name(port1);
	const char* port2_name = sp_get_port_name(port2);

	if (open(port1) != 0) return 1;
	if (open(port2) != 0) goto close_1;

	struct sp_event_set* event_set = NULL;
	if (sp_new_event_set(&event_set) != SP_OK) {
		fprintf(stderr, "Failed to create event set.\n");
		goto close_2;
	}

	if (
		sp_add_port_events(
			event_set,
			port1,
			SP_EVENT_RX_READY | SP_EVENT_TX_READY
		) != SP_OK
	) {
		fprintf(stderr, "Failed to add port events for \"%s\".\n", port1_name);
		goto event_set;
	}

	if (
		sp_add_port_events(
			event_set,
			port2,
			SP_EVENT_RX_READY | SP_EVENT_TX_READY
		) != SP_OK
	) {
		fprintf(stderr, "Failed to add port events for \"%s\".\n", port2_name);
		goto event_set;
	}

	struct rw_buffer buf1, buf2;
	init_buffer(&buf1);
	init_buffer(&buf2);

	clock_t last_active1 = 0, last_active2 = 0;
	while (sp_wait(event_set, 0) == SP_OK) {
		int result = read_some(port1, &buf1);
		if (result > 0) {
			clock_t now = clock();
			if (((now - last_active1) / CLOCKS_PER_SEC) > 0.1 || last_active2 > last_active1) {
				time_t t = time(NULL);
				printf("\n\n%s %s to %s:\n", asctime(localtime(&t)), port1_name, port2_name);
			}
			last_active1 = now;

			int i;
			for (i = buf1.head - result; i < buf1.head; ++i) {
				printf("%02X", buf1.data[i]);
			}
		}
		else if (result < 0) fprintf(stderr, "Read error for \"%s\".\n", port1_name);

		result = read_some(port2, &buf2);
		if (result > 0) {
			clock_t now = clock();
			if (((now - last_active2) / CLOCKS_PER_SEC) > 0.1 || last_active1 > last_active2) {
				time_t t = time(NULL);
				printf("\n\n%s%s to %s:\n", asctime(localtime(&t)), port2_name, port1_name);
			}
			last_active2 = now;

			int i;
			for (i = buf2.head - result; i < buf2.head; ++i) {
				printf("%02X", buf2.data[i]);
			}
		}
		else if (result < 0) fprintf(stderr, "Read error for \"%s\".\n", port2_name);

		result = write_some(port1, &buf2);
		if (result < 0) fprintf(stderr, "Write error for \"%s\".\n", port1_name);

		result = write_some(port2, &buf1);
		if (result < 0) fprintf(stderr, "Write error for \"%s\".\n", port2_name);
	}

event_set:
	sp_free_event_set(event_set);
close_2:
	sp_close(port2);
close_1:
	sp_close(port1);
	return 0;
}
