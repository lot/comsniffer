#Try to find libserialport
#If found, imported target libserialport::libserialport is defined
#Defines the following variables
#libserialport_FOUND
#libserialport_INCLUDE_DIRS
#libserialport_LIBRARIES

find_path(
	libserialport_INCLUDE_DIR
	NAMES libserialport.h
)

find_library(
	libserialport_LIBRARY
	NAMES serialport libserialport
)

set(libserialport_INCLUDE_DIRS ${libserialport_INCLUDE_DIR})
set(libserialport_LIBRARIES ${libserialport_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
	libserialport
	DEFAULT_MSG
	libserialport_LIBRARY
	libserialport_INCLUDE_DIR
)

mark_as_advanced(libserialport_INCLUDE_DIR libserialport_LIBRARY)

if(libserialport_FOUND)
	add_library(libserialport::libserialport UNKNOWN IMPORTED)
	set_target_properties(
		libserialport::libserialport
		PROPERTIES
			IMPORTED_LOCATION ${libserialport_LIBRARIES}
			INTERFACE_INCLUDE_DIRECTORIES ${libserialport_INCLUDE_DIRS}
	)
endif()
